Provides integration between Domain Access and File Entity, allowing files to 
be assigned to domains. This is only useful for sites requiring uploaded media 
(eg via Media module) to be assigned to domains.